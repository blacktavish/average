package com.demo.springboot.service;

import com.demo.springboot.dto.DigitsDto;
import org.springframework.stereotype.Service;

@Service
public class AverageImpl implements  Average{

    public AverageImpl(){

    }
    @Override
    public DigitsDto Calculate(float[] digits) {
        float avg;
        float sum = 0;
        for(int i = 0; i < digits.length; i++){

            sum += digits[i];
        }
        avg = sum/digits.length;
        return new DigitsDto(avg);
    }
}
