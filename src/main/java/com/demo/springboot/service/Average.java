package com.demo.springboot.service;

import com.demo.springboot.dto.DigitsDto;

public interface Average {
    DigitsDto Calculate(float[] digits);
}
